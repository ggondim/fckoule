﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Santander.Web.Api.Controllers
{
    public class HomeController : Controller
    {
        public HomeController(object usuarioServico)
        {
            this._usuarioServico = usuarioServico;
        }

        public ActionResult Index()
        {
            return View();
        }

        private object _usuarioServico { get; set; }
    }
}

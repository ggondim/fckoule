﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject.Modules;
using Ninject.Web.Common;
using Santander.Lib.Dados;
using Santander.Dados.Entity;
using System.Configuration;

namespace Santander.Web.Api
{
    public class SantanderModulo : NinjectModule
    {
        public override void Load()
        {
            // User Id=pessudafcamara;Password=santander;

            this.Bind<IDbContexto>()
                .ToConstructor(_ => new SantanderContexto(ConfigurationManager.ConnectionStrings["cn-mssql"].ConnectionString))                
                .InRequestScope();
            this.Bind(typeof(IRepositorio<>)).To(typeof(EfRepositorio<>))
                .InRequestScope();

            //this.Bind<IUsuarioServico>().To<UsuarioServico>();
        }
    }
}

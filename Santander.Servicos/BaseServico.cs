﻿using System;
using System.Collections.Generic;
using System.Linq;
using Santander.Dominio;
using Santander.Lib.Dados;

namespace Santander.Servicos
{
    public abstract class BaseServico<T> : IBaseServico<T> where T : class
    {
        private readonly IRepositorio<T> _repositorio;

        protected BaseServico(IRepositorio<T> repositorio)
        {
            _repositorio = repositorio;
        }

        public virtual RetornoStatus<T> Registrar(T item)
        {
            var retorno = Validar(item);
            if (retorno.TipoStatus != ERetornoStatus.Sucesso) return retorno;

            try
            {
                retorno.Item = _repositorio.Inserir(item);
                retorno.TipoStatus = ERetornoStatus.Sucesso;
                retorno.Mensagens.Add("Registrado com sucesso.");
            }
            catch (Exception ex)
            {
                retorno.TipoStatus = ERetornoStatus.Erro;
                retorno.Mensagens.Add(ex.Message);
            }

            return retorno;
        }

        public virtual T Obter(int id)
        {
            return _repositorio.Obter(id);
        }

        public virtual IEnumerable<T> ListarTodos()
        {
            return _repositorio.Pesquisar().ToList();
        }

        public virtual RetornoStatus<T> Editar(T item)
        {
            var retorno = Validar(item);
            retorno.Item = item;

            if (retorno.TipoStatus != ERetornoStatus.Sucesso) return retorno;

            try
            {
                _repositorio.Atualizar(item);
                retorno.TipoStatus = ERetornoStatus.Sucesso;
                retorno.Mensagens.Add("Editado com sucesso.");
            }
            catch (Exception ex)
            {
                retorno.TipoStatus = ERetornoStatus.Erro;
                retorno.Mensagens.Add(ex.Message);
            }

            return retorno;
        }

        public virtual RetornoStatus<T> Apagar(T item)
        {
            var retorno = Validar(item);
            retorno.Item = item;

            if (retorno.TipoStatus != ERetornoStatus.Sucesso) return retorno;

            try
            {
                _repositorio.Apagar(item);
                retorno.TipoStatus = ERetornoStatus.Sucesso;
                retorno.Mensagens.Add("Apagado com sucesso.");
            }
            catch (Exception ex)
            {
                retorno.TipoStatus = ERetornoStatus.Erro;
                retorno.Mensagens.Add(ex.Message);
            }

            return retorno;
        }

        public virtual RetornoStatus<T> Validar(T item)
        {
            return new RetornoStatus<T>
            {
                TipoStatus = ERetornoStatus.Sucesso
            };
        }
    }
}

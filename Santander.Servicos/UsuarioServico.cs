﻿using Santander.Dominio;
using Santander.Lib.Dados;

namespace Santander.Servicos
{
    class UsuarioServico : BaseServico<Usuario>, IUsuarioServico
    {
        public UsuarioServico(IRepositorio<Usuario> repositorio) : base(repositorio) { }
    }
}

﻿using Santander.Dominio;
using Santander.Lib.Dados;

namespace Santander.Servicos
{
    class PerfilServico : BaseServico<Perfil>, IPerfilServico
    {
        public PerfilServico(IRepositorio<Perfil> repositorio) : base(repositorio) { }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Santander.Lib.Dados
{
    public interface IDbContexto
    {
        IDbSet<TEntidade> Set<TEntidade>()
            where TEntidade : class;

        int SaveChanges();

        IEnumerable<TElemento> SqlQuery<TElemento>(string sql, params object[] parametros);

        DbEntityEntry<TEntidade> Entry<TEntidade>(TEntidade entidade)
             where TEntidade : class;
    }
}

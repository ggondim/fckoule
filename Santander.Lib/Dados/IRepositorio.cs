﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Santander.Lib.Dados
{
    public interface IRepositorio<TEntidade>
        where TEntidade : class
    {
        TEntidade Inserir(TEntidade entidade);
        TEntidade Obter(params object[] id);
        bool Atualizar(TEntidade entidade);
        bool Apagar(TEntidade entidade);
        IQueryable<TEntidade> Pesquisar();
    }
}

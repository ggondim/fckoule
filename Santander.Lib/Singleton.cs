﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System
{
    /// <summary>
    /// Provê geração de classes uninstanciáveis no modelo Singleton.
    /// </summary>
    /// <typeparam name="T">Tipo da classe a ser uninstanciada.</typeparam>
    public sealed class Singleton<T> where T : class, new()
    {
        private static T _instancia;

        public static T ObterInstancia()
        {
            lock (typeof(T))
            {
                if (_instancia == null) _instancia = new T();
                return _instancia;
            }
        }
    }
}

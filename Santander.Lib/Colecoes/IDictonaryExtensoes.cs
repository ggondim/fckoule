﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace System
{
    public static class IDictonaryExtensoes
    {
        public static NameValueCollection ConverterParaNameValue(this IDictionary<string, string> dicionario)
        {
            NameValueCollection retorno = new NameValueCollection(dicionario.Count);
            foreach (var item in dicionario)
            {
                retorno.Add(item.Key, item.Value);
            }
            return retorno;
        }
    }
}

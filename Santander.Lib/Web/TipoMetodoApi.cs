﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Santander.Lib.Web
{
    public enum TipoMetodoApi
    {
        GET,
        POST,
        PUT,
        PATCH,
        HEADER
    }
}

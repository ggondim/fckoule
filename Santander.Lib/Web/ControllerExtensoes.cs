﻿using Santander.Lib.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace Santander
{
    public static class ControllerExtensoes
    {
        public static T RequisicaoApi<T>(this Controller controller, string urlMetodoApi)
            where T : class
        {
            return controller.RequisicaoApi<T>(urlMetodoApi, TipoMetodoApi.GET, new Dictionary<string, string>());
        }
        public static T RequisicaoApi<T>(this Controller controller, string urlMetodoApi, TipoMetodoApi metodo)
            where T : class
        {
            return controller.RequisicaoApi<T>(urlMetodoApi, metodo, new Dictionary<string, string>());
        }

        public static T RequisicaoApi<T>(this Controller controller, string urlMetodoApi, IDictionary<string, string> queryString)
            where T : class
        {
            return controller.RequisicaoApi<T>(urlMetodoApi, TipoMetodoApi.GET, queryString);
        }

        public static T RequisicaoApi<T>(this Controller controller, string urlMetodoApi, TipoMetodoApi metodo, IDictionary<string, string> queryString)
            where T : class
        {
            return SantanderApi.Requisicao<T>(urlMetodoApi, metodo, queryString);
        }
    }
}

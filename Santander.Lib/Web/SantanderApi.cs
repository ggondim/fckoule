﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Santander.Lib.Web;
using System.Configuration;

namespace Santander
{
    public class SantanderApi
    {
        //public static T RequisicaoGet<T>(string urlMetodoApi, IDictionary<string, string> queryString)
        //    where T : class
        //{
        //    string mascaraParametro = "{0}:{1}";
        //    string urlRequisicao = urlMetodoApi + (queryString.Count > 0 ? "?" : string.Empty);
        //    foreach (var item in queryString)
        //    {
        //        urlRequisicao += string.Format(mascaraParametro, item.Key, item.Value);
        //    }
        //    return RequisicaoGet<T>(urlRequisicao);
        //}

        //public static T RequisicaoGet<T>(string urlMetodoApi)
        //    where T : class
        //{
        //    WebRequest requisicao = WebRequest.Create(urlMetodoApi);
        //    WebResponse resposta = requisicao.GetResponse();
        //    string respostaApi = string.Empty;
        //    using (StreamReader leitor = new StreamReader(resposta.GetResponseStream()))
        //    {
        //        respostaApi = leitor.ReadToEnd();
        //    }
        //    return JsonConvert.DeserializeObject<T>(respostaApi);
        //}

        public static T Requisicao<T>(string urlMetodoApi)
            where T : class
        {
            return Requisicao<T>(urlMetodoApi, TipoMetodoApi.GET, new Dictionary<string, string>());
        }
        public static T Requisicao<T>(string urlMetodoApi, TipoMetodoApi metodo)
            where T : class
        {
            return Requisicao<T>(urlMetodoApi, metodo, new Dictionary<string, string>());
        }

        public static T Requisicao<T>(string urlMetodoApi, IDictionary<string, string> queryString)
            where T : class
        {
            return Requisicao<T>(urlMetodoApi, TipoMetodoApi.GET, queryString);
        }

        public static T Requisicao<T>(string urlMetodoApi, TipoMetodoApi metodo, IDictionary<string, string> queryString)
            where T : class
        {
            string urlRequisicao = string.Empty;
            if (urlMetodoApi.ToLower().Contains("http"))
            {
                urlRequisicao = urlMetodoApi;
            }
            else
            {
                string urlRaiz = ConfigurationManager.AppSettings["UrlApi"];
                if (!urlRaiz.EndsWith("/"))
                {
                    urlRaiz += "/";
                }
                if (urlMetodoApi.StartsWith("/"))
                {
                    urlMetodoApi = urlMetodoApi.Substring(1);
                }
                urlRequisicao = urlRaiz + urlMetodoApi;
            }

            byte[] resposta = null;
            using (WebClient client = new WebClient())
            {
                resposta = client.UploadValues(urlRequisicao, metodo.ToString(), queryString.ConverterParaNameValue());
            }
            return JsonConvert.DeserializeObject<T>(Encoding.Default.GetString(resposta));
        }
    }
}

﻿using System;
using System.Data.Entity;
using System.Linq;
using Santander.Lib.Dados;

namespace Santander.Dados.Entity
{
    public class EfRepositorio<TEntidade>
        : IRepositorio<TEntidade>
        where TEntidade : class
    {
        private readonly IDbContexto _contexto;
        private IDbSet<TEntidade> _entidades;

        protected virtual IDbSet<TEntidade> Entidades
        {
            get
            {
                if (_entidades == null)
                    _entidades = _contexto.Set<TEntidade>();
                return _entidades;
            }
        }

        public EfRepositorio(IDbContexto contexto)
        {
            this._contexto = contexto;
        }

        public TEntidade Inserir(TEntidade entidade)
        {
            if (entidade == null)
                throw new ArgumentException("entidade");

            this.Entidades.Add(entidade);
            this._contexto.SaveChanges();
            return entidade;
        }

        public TEntidade Obter(params object[] identidade)
        {
            return this.Entidades.Find(identidade);
        }

        public bool Atualizar(TEntidade entidade)
        {
            if (entidade == null)
                throw new ArgumentException("entidade");

            this._contexto.Entry<TEntidade>(entidade).State = EntityState.Modified;
            return this._contexto.SaveChanges() > 0;
        }

        public bool Apagar(TEntidade entidade)
        {
            if (entidade == null)
                throw new ArgumentException("entidade");

            this.Entidades.Remove(entidade);
            return this._contexto.SaveChanges() > 0;
        }

        public IQueryable<TEntidade> Pesquisar()
        {
            return this.Entidades;
        }
    }
}

﻿using Santander.Lib.Dados;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Santander.Dados.Entity
{
    public class SantanderContexto : DbContext, IDbContexto
    {
        public SantanderContexto(string nomeOuConnectionString)
            : base(nomeOuConnectionString, Singleton<SantanderModelo>.ObterInstancia().ModeloCompilado)
        {
            this.Database.CreateIfNotExists();
        }

        public new IDbSet<TEntidade> Set<TEntidade>() 
            where TEntidade : class
        {
            return base.Set<TEntidade>();
        }

        public IEnumerable<TElemento> SqlQuery<TElemento>(string sql, params object[] parametros)
        {
            return this.Database.SqlQuery<TElemento>(sql, parametros);
        }
    }
}

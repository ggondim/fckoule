using Santander.Dominio;
using System.Data.Entity.ModelConfiguration;

namespace Santander.Dados.Entity.Mapeamentos
{
    public class ParceiroMapeamento : EntityTypeConfiguration<Parceiro>
    {
        public ParceiroMapeamento()
        {
            // Primary Key
            this.HasKey(t => t.IdParceiro);

            // Properties
            this.Property(t => t.Nome)
                .IsRequired()
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("TB_Parceiro");
            this.Property(t => t.IdParceiro).HasColumnName("IdParceiro");
            this.Property(t => t.Nome).HasColumnName("Nome");
        }
    }
}

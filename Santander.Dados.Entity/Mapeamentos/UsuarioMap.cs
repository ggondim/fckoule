using Santander.Dominio;
using System.Data.Entity.ModelConfiguration;

namespace Santander.Dados.Entity.Mapeamentos
{
    public class UsuarioMapeamento : EntityTypeConfiguration<Usuario>
    {
        public UsuarioMapeamento()
        {
            // Primary Key
            this.HasKey(t => t.IdUsuario);

            // Properties
            this.Property(t => t.Nome)
                .HasMaxLength(100);

            this.Property(t => t.UserName)
                .HasMaxLength(100);

            this.Property(t => t.Senha)
                .HasMaxLength(500);

            this.Property(t => t.Email)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("TB_Usuario");
            this.Property(t => t.IdUsuario).HasColumnName("IdUsuario");
            this.Property(t => t.Nome).HasColumnName("Nome");
            this.Property(t => t.IdTipoCadastro).HasColumnName("IdTipoCadastro");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.Senha).HasColumnName("Senha");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.DataAlteracaoSenha).HasColumnName("DataAlteracaoSenha");
            this.Property(t => t.DataUltimoLogin).HasColumnName("DataUltimoLogin");
            this.Property(t => t.QuantidadeTentativaAcesso).HasColumnName("QuantidadeTentativaAcesso");
            this.Property(t => t.QuantidadeAcesso).HasColumnName("QuantidadeAcesso");
            this.Property(t => t.IdAplicacao).HasColumnName("IdAplicacao");
            this.Property(t => t.DataLimiteAcesso).HasColumnName("DataLimiteAcesso");
            this.Property(t => t.DataBloqueadoAte).HasColumnName("DataBloqueadoAte");
            this.Property(t => t.FlagDesligado).HasColumnName("Flag_Desligado");
            this.Property(t => t.IdUsuarioCadastro).HasColumnName("IdUsuarioCadastro");
            this.Property(t => t.IdStatus).HasColumnName("IdStatus");
            this.Property(t => t.DataInclusao).HasColumnName("DataInclusao");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
        }
    }
}

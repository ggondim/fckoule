using Santander.Dominio;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Santander.Dados.Entity.Mapeamentos
{
    public class CategoriaMapeamento : EntityTypeConfiguration<Categoria>
    {
        public CategoriaMapeamento()
        {
            // Primary Key
            this.HasKey(t => t.IdCategoria);

            // Properties
            this.Property(t => t.Nome)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.Cor)
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("TB_Categoria");
            this.Property(t => t.IdCategoria).HasColumnName("IdCategoria");
            this.Property(t => t.IdPai).HasColumnName("IdPai");
            this.Property(t => t.Nome).HasColumnName("Nome");
            this.Property(t => t.Cor).HasColumnName("Cor");

            // Relationships
            this.HasOptional(t => t.Categoria2)
                .WithMany(t => t.Categoria1)
                .HasForeignKey(d => d.IdPai);

        }
    }
}

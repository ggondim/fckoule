using Santander.Dominio;
using System.Data.Entity.ModelConfiguration;

namespace Santander.Dados.Entity.Mapeamentos
{
    public class BlogPostTagMapeamento : EntityTypeConfiguration<BlogPostTag>
    {
        public BlogPostTagMapeamento()
        {
            // Primary Key
            this.HasKey(t => t.IdBlogPostTag);

            // Properties
            // Table & Column Mappings
            this.ToTable("TB_BlogPostTag");
            this.Property(t => t.IdBlogPostTag).HasColumnName("IdBlogPostTag");
            this.Property(t => t.IdBlogPost).HasColumnName("IdBlogPost");
            this.Property(t => t.IdBlogTag).HasColumnName("IdBlogTag");

            // Relationships
            this.HasRequired(t => t.BlogPost)
                .WithMany(t => t.BlogPostTag)
                .HasForeignKey(d => d.IdBlogPost);
            this.HasRequired(t => t.BlogTag)
                .WithMany(t => t.BlogPostTag)
                .HasForeignKey(d => d.IdBlogTag);

        }
    }
}

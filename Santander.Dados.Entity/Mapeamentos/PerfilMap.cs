using Santander.Dominio;
using System.Data.Entity.ModelConfiguration;

namespace Santander.Dados.Entity.Mapeamentos
{
    public class PerfilMapeamento : EntityTypeConfiguration<Perfil>
    {
        public PerfilMapeamento()
        {
            // Primary Key
            this.HasKey(t => t.IdPerfil);

            // Properties
            this.Property(t => t.Nome)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TB_Perfil");
            this.Property(t => t.IdPerfil).HasColumnName("IdPerfil");
            this.Property(t => t.Nome).HasColumnName("Nome");
            this.Property(t => t.IdStatus).HasColumnName("IdStatus");
            this.Property(t => t.DataInclusao).HasColumnName("DataInclusao");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasMany(t => t.Permissao)
                .WithMany(t => t.Perfil)
                .Map(m =>
                    {
                        m.ToTable("TB_PerfilPermissao");
                        m.MapLeftKey("IdPerfil");
                        m.MapRightKey("IdPermissao");
                    });

            this.HasMany(t => t.Usuario)
                .WithMany(t => t.Perfil)
                .Map(m =>
                    {
                        m.ToTable("TB_UsuarioPerfil");
                        m.MapLeftKey("IdPerfil");
                        m.MapRightKey("IdUsuario");
                    });


        }
    }
}

using Santander.Dominio;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Santander.Dados.Entity.Mapeamentos
{
    public class BlogPostMapeamento : EntityTypeConfiguration<BlogPost>
    {
        public BlogPostMapeamento()
        {
            // Primary Key
            this.HasKey(t => t.IdBlogPost);

            // Properties
            this.Property(t => t.Titulo)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.TituloDestaque)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.Texto)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("TB_BlogPost");
            this.Property(t => t.IdBlogPost).HasColumnName("IdBlogPost");
            this.Property(t => t.Titulo).HasColumnName("Titulo");
            this.Property(t => t.TituloDestaque).HasColumnName("TituloDestaque");
            this.Property(t => t.Texto).HasColumnName("Texto");
            this.Property(t => t.DataInclusao).HasColumnName("DataInclusao");
            this.Property(t => t.IdUsuario).HasColumnName("IdUsuario");
            this.Property(t => t.IdBlogueiro).HasColumnName("IdBlogueiro");

            // Relationships
            this.HasRequired(t => t.Blogueiro)
                .WithMany(t => t.BlogPost)
                .HasForeignKey(d => d.IdBlogueiro);
            this.HasRequired(t => t.Usuario)
                .WithMany(t => t.BlogPost)
                .HasForeignKey(d => d.IdUsuario);

        }
    }
}

using Santander.Dominio;
using System.Data.Entity.ModelConfiguration;

namespace Santander.Dados.Entity.Mapeamentos
{
    public class BlogueiroMapeamento : EntityTypeConfiguration<Blogueiro>
    {
        public BlogueiroMapeamento()
        {
            // Primary Key
            this.HasKey(t => t.IdBlogueiro);

            // Properties
            this.Property(t => t.Nome)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.Especialidade)
                .IsRequired()
                .HasMaxLength(300);

            this.Property(t => t.Descricao)
                .IsRequired();

            this.Property(t => t.Imagem)
                .HasMaxLength(300);

            // Table & Column Mappings
            this.ToTable("TB_Blogueiro");
            this.Property(t => t.IdBlogueiro).HasColumnName("IdBlogueiro");
            this.Property(t => t.Nome).HasColumnName("Nome");
            this.Property(t => t.Especialidade).HasColumnName("Especialidade");
            this.Property(t => t.Descricao).HasColumnName("Descricao");
            this.Property(t => t.Imagem).HasColumnName("Imagem");
        }
    }
}

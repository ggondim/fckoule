using Santander.Dominio;
using System.Data.Entity.ModelConfiguration;

namespace Santander.Dados.Entity.Mapeamentos
{
    public class BlogTagMapeamento : EntityTypeConfiguration<BlogTag>
    {
        public BlogTagMapeamento()
        {
            // Primary Key
            this.HasKey(t => t.IdBlogTag);

            // Properties
            this.Property(t => t.Tag)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("TB_BlogTag");
            this.Property(t => t.IdBlogTag).HasColumnName("IdBlogTag");
            this.Property(t => t.Tag).HasColumnName("Tag");
        }
    }
}

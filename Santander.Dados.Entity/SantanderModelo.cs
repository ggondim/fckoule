﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Santander.Dados.Entity
{
    public class SantanderModelo
    {        
        public DbModelBuilder ModelBuilder { get; set; }
        public DbCompiledModel ModeloCompilado { get; private set; }
        public DbProviderInfo Provider { get; set; }

        public SantanderModelo()
        {
            this.Provider = new DbProviderInfo("System.Data.SqlClient", "2008");
            this.ModelBuilder = new DbModelBuilder();
            this.ConstruirModelo();
            this.ModeloCompilado = this.ModelBuilder
                .Build(this.Provider)
                .Compile();
        }

        public void ConstruirModelo()
        {

            IEnumerable<Type> mapeamentos = Assembly.GetExecutingAssembly()
                .GetTypes()
                .Where(t => !string.IsNullOrEmpty(t.Namespace))
                .Where(t => t.BaseType != null
                    && t.BaseType.IsGenericType
                    && t.BaseType.GetGenericTypeDefinition() == typeof(EntityTypeConfiguration<>));
            foreach (Type mapeamento in mapeamentos)
            {
                dynamic config = Activator.CreateInstance(mapeamento);
                this.ModelBuilder.Configurations.Add(config);
            }
        }
    }
}

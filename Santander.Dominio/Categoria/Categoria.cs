using System;
using System.Collections.Generic;

namespace Santander.Dominio
{
    public partial class Categoria
    {
        public Categoria()
        {
            this.Categoria1 = new List<Categoria>();
        }

        public int IdCategoria { get; set; }
        public int? IdPai { get; set; }
        public string Nome { get; set; }
        public string Cor { get; set; }
        public virtual ICollection<Categoria> Categoria1 { get; set; }
        public virtual Categoria Categoria2 { get; set; }
    }
}

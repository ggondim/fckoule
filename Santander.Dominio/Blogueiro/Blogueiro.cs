using System.Collections.Generic;

namespace Santander.Dominio
{
    public partial class Blogueiro
    {
        public Blogueiro()
        {
            this.BlogPost = new List<BlogPost>();
        }

        public int IdBlogueiro { get; set; }
        public string Nome { get; set; }
        public string Especialidade { get; set; }
        public string Descricao { get; set; }
        public string Imagem { get; set; }
        public virtual ICollection<BlogPost> BlogPost { get; set; }
    }
}

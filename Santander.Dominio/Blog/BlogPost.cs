using System;
using System.Collections.Generic;

namespace Santander.Dominio
{
    public partial class BlogPost
    {
        public BlogPost()
        {
            this.BlogPostTag = new List<BlogPostTag>();
        }

        public int IdBlogPost { get; set; }
        public string Titulo { get; set; }
        public string TituloDestaque { get; set; }
        public string Texto { get; set; }
        public System.DateTime DataInclusao { get; set; }
        public int IdUsuario { get; set; }
        public int IdBlogueiro { get; set; }
        public virtual Blogueiro Blogueiro { get; set; }
        public virtual Usuario Usuario { get; set; }
        public virtual ICollection<BlogPostTag> BlogPostTag { get; set; }
    }
}

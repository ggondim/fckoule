using System.Collections.Generic;

namespace Santander.Dominio
{
    public partial class BlogTag
    {
        public BlogTag()
        {
            this.BlogPostTag = new List<BlogPostTag>();
        }

        public int IdBlogTag { get; set; }
        public string Tag { get; set; }
        public virtual ICollection<BlogPostTag> BlogPostTag { get; set; }
    }
}

namespace Santander.Dominio
{
    public partial class BlogPostTag
    {
        public int IdBlogPostTag { get; set; }
        public int IdBlogPost { get; set; }
        public int IdBlogTag { get; set; }
        public virtual BlogPost BlogPost { get; set; }
        public virtual BlogTag BlogTag { get; set; }
    }
}

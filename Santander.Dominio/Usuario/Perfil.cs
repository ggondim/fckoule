using System;
using System.Collections.Generic;

namespace Santander.Dominio
{
    public partial class Perfil
    {
        public Perfil()
        {
            this.Permissao = new List<Permissao>();
            this.Usuario = new List<Usuario>();
        }

        public int IdPerfil { get; set; }
        public string Nome { get; set; }
        public int IdStatus { get; set; }
        public System.DateTime DataInclusao { get; set; }
        public System.DateTime DataAlteracao { get; set; }
        public virtual ICollection<Permissao> Permissao { get; set; }
        public virtual ICollection<Usuario> Usuario { get; set; }
    }
}

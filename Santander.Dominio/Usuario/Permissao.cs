using System.Collections.Generic;

namespace Santander.Dominio
{
    public partial class Permissao
    {
        public Permissao()
        {
            Perfil = new List<Perfil>();
        }

        public int IdPermissao { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public System.DateTime DataInclusao { get; set; }
        public System.DateTime DataAlteracao { get; set; }
        public virtual ICollection<Perfil> Perfil { get; set; }
    }
}

using System;
using System.Collections.Generic;

namespace Santander.Dominio
{
    public class Usuario
    {
        public Usuario()
        {
            this.BlogPost = new List<BlogPost>();
            this.Perfil = new List<Perfil>();
        }

        public int IdUsuario { get; set; }
        public string Nome { get; set; }
        public int IdTipoCadastro { get; set; }
        public string UserName { get; set; }
        public string Senha { get; set; }
        public string Email { get; set; }
        public DateTime? DataAlteracaoSenha { get; set; }
        public DateTime? DataUltimoLogin { get; set; }
        public int QuantidadeTentativaAcesso { get; set; }
        public int QuantidadeAcesso { get; set; }
        public int? IdAplicacao { get; set; }
        public DateTime? DataLimiteAcesso { get; set; }
        public DateTime? DataBloqueadoAte { get; set; }
        public bool? FlagDesligado { get; set; }
        public int? IdUsuarioCadastro { get; set; }
        public int IdStatus { get; set; }
        public System.DateTime DataInclusao { get; set; }
        public System.DateTime DataAlteracao { get; set; }
        public virtual ICollection<BlogPost> BlogPost { get; set; }
        public virtual ICollection<Perfil> Perfil { get; set; }
    }
}

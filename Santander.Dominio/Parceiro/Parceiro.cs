using System;
using System.Collections.Generic;

namespace Santander.Dominio
{
    public partial class Parceiro
    {
        public int IdParceiro { get; set; }
        public string Nome { get; set; }
    }
}

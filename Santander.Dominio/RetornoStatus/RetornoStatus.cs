using System.Collections.Generic;

namespace Santander.Dominio
{
    public class RetornoStatus<T>
    {
        public ERetornoStatus TipoStatus { get; set; }
        public List<string> Mensagens { get; set; }
        public T Item { get; set; }
    }

    public enum ERetornoStatus
    {
        Erro,
        Sucesso
    }
}

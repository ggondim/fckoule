﻿using System.Collections.Generic;

namespace Santander.Dominio
{
    public interface IBaseServico<T>
    {
        RetornoStatus<T> Registrar(T item);
        RetornoStatus<T> Editar(T item);
        RetornoStatus<T> Apagar(T item);
        T Obter(int id);
        IEnumerable<T> ListarTodos();
        RetornoStatus<T> Validar(T item);
    }
}
